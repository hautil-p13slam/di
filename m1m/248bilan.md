% Bilan Bloc23DI
% m1m et p13slam
% fin du semestre 3/4 (2022)

# Le vendredi 2 décembre avant 11:30

~~~
 fred   main  …  DIblc_  slam  di  git pull
remote: Enumerating objects: 17, done.
remote: Counting objects: 100% (17/17), done.
remote: Compressing objects: 100% (13/13), done.
remote: Total 16 (delta 3), reused 0 (delta 0), pack-reused 0
Dépaquetage des objets: 100% (16/16), 16.47 Kio | 4.12 Mio/s, fait.
Depuis gitlab.com:hautil-p13slam/di
   81d2d62..e228ac4  main       -> origin/main
Mise à jour 81d2d62..e228ac4
Fast-forward
 DargentNils/236access.log         |  20 ++++++++++++++++++++
 HustacheLucas/238acces.log        |  11 +++++++++++
 HustacheLucas/ll.png              | Bin 0 -> 15269 bytes
 Marie-RoseAugustin/238_access.log |  10 ++++++++++
 4 files changed, 41 insertions(+)
 create mode 100644 DargentNils/236access.log
 create mode 100644 HustacheLucas/238acces.log
 create mode 100644 HustacheLucas/ll.png
 create mode 100644 Marie-RoseAugustin/238_access.log

~~~

* et en image

![impression d'écran fred@vela](enImage.png)

# Le même jour vers 12:20 (un complément)

~~~
 fred   main  …  DIblc_  slam  di  git pull
remote: Enumerating objects: 17, done.
remote: Counting objects: 100% (17/17), done.
remote: Compressing objects: 100% (14/14), done.
remote: Total 16 (delta 5), reused 3 (delta 0), pack-reused 0
Dépaquetage des objets: 100% (16/16), 2.04 Kio | 695.00 Kio/s, fait.
Depuis gitlab.com:hautil-p13slam/di
   d5f8533..95ba6ba  main       -> origin/main
Mise à jour d5f8533..95ba6ba
Fast-forward
 Abdallah_TALEB/.gitkeep      |  0
 Abdallah_TALEB/accesslog.txt | 22 ++++++++++++++++++++++
 Bedis/.gitkeep               |  0
 MercellusSebastien/.gitkeep  |  0
 MercellusSebastien/acces.log | 10 ++++++++++
 5 files changed, 32 insertions(+)
 create mode 100644 Abdallah_TALEB/.gitkeep
 create mode 100644 Abdallah_TALEB/accesslog.txt
 create mode 100644 Bedis/.gitkeep
 create mode 100644 MercellusSebastien/.gitkeep
 create mode 100644 MercellusSebastien/acces.log

~~~

* et en une seconde image

![impression d'écran fred@vela no 2](Image2.png)
